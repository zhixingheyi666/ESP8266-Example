#include <ESP8266WiFi.h>
#include <DHT.h>

WiFiServer server(23);

DHT sensor(D1, DHT11);

void setup()
{
    Serial.begin(115200);
    WiFi.mode(WIFI_STA);
    WiFi.begin("LIUYU", "12345678");
    while (!WiFi.isConnected())
    {
        delay(500);
        Serial.print('.');
    }
    Serial.println(WiFi.localIP());
    pinMode(D1, OUTPUT);
    digitalWrite(D1, HIGH); // Relay off
    server.begin();
    sensor.begin();
}

void loop()
{
    WiFiClient client = server.available();
    if (!client)
    {
        return;
    }

    Serial.print(client.remoteIP());
    Serial.println(" connected");

    while (client.connected())
    {
        float temp = sensor.readTemperature();
        float humi = sensor.readHumidity();
        String tempstr = String("Temperature: ") + String(temp) + String("\r\n");
        String humistr = String("Humidity: ") + String(humi) + String("\r\n");
        client.write(tempstr.c_str());
        client.write(humistr.c_str());
        delay(3000);
    }
}
