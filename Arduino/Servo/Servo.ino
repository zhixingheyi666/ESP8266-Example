#include <Servo.h>

Servo servo;

void setup() {
    Serial.begin(115200);
    servo.attach(D7);
    servo.write(0);
}

void loop() {
    if (Serial.available())
    {
        servo.write(Serial.readString().toInt());
    }
}
