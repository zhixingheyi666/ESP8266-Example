#include <DHT.h>

DHT sensor(D4, DHT11);

void setup() {
    Serial.begin(115200);
    sensor.begin();
}

void loop() {
    float temp = sensor.readTemperature();
    float humi = sensor.readHumidity();
    String s = "Temperature: " + String(temp, 1) + ", Humidity: " + String(humi, 0) + "%";
    Serial.println(s);
    delay(1000);
}
