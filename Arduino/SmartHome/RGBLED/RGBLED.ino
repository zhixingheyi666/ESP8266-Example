#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <Adafruit_NeoPixel.h>
#include "wificonfig.h"

#define LED_PIN D1
#define LED_NUM 8

const char* mqttServer = "192.168.121.156";
const char* topic_light_color = "hqyjxa/light/color";
const char* will_topic = "hqyjxa/light/online";
WiFiClient wificlient;
PubSubClient client(wificlient);

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(LED_NUM, LED_PIN, NEO_GRB + NEO_KHZ800);

//Arduinojson库如何配合MQTT使用
//https://arduinojson.org/v6/faq/how-to-use-arduinojson-with-pubsubclient/
void callback(char* topic, byte* payload, unsigned int length)
{
    Serial.println(topic);
    Serial.println(length);
    for (unsigned int i = 0; i < length; i++)
    {
        Serial.print((char)payload[i]);
    }
    Serial.println("");

    //处理light_color主题
    if (strcmp(topic, topic_light_color) == 0)
    {
        //在栈上分配json对象
        StaticJsonDocument<1024> obj;
        //解析payload中的json对象
        //{"r":220,"g":255,"b":230,"a":1}
        deserializeJson(obj, payload, length);
        for (int i = 0; i < LED_NUM; i++) {
            pixels.setPixelColor(i, pixels.Color(obj["r"].as<int>(),
                                                 obj["g"].as<int>(),
                                                 obj["b"].as<int>()));
            pixels.show();
        }
    }
}

void reconnect()
{
    //连接MQTT代理，客户端ID，用户名，密码，遗嘱主题，遗嘱QoS，遗嘱保留，遗嘱内容
    //连接成功后，将online主题（遗嘱）设置为1，表示设备上线，订阅者收到通知
    //连接非正常断开后，online内容会被代理设置为0，表示设备下线，订阅者收到通知。
    //https://pubsubclient.knolleary.net/api.html
    char clientID[16];
    sprintf(clientID, "hqyjxa-%08x", ESP.getChipId());
    if (client.connect(clientID, NULL, NULL, will_topic, 1, 1, "0")) {
        Serial.print(clientID);
        Serial.println(" connected");
        client.subscribe(topic_light_color, 1);
        client.publish(will_topic, "1", true);
        digitalWrite(LED_BUILTIN, LOW); //LED On
    } else {
        Serial.print("connect failed ");
        Serial.print(client.state());
        Serial.println(", try again in 1 second");
        digitalWrite(LED_BUILTIN, HIGH); //LED Off
        delay(1000);
    }
}

void setup() {
    Serial.begin(115200);
    autoConfig();
    client.setServer(mqttServer, 1883);
    client.setCallback(callback);
    pixels.begin();
}

void loop() {
    if (!client.connected()) {
        reconnect();
    }
    client.loop();
}
